import java.io.*;
import java.math.*;
import java.net.*;
import java.util.*;

public class RTSP {

	private String ServerHost;
	private int RTP_PORT;
	private int RTSP_PORT;
	Socket RTSPsocket; //socket used to send/receive RTSP messages
	
	//TRSP seqnum range
	final static int MIN_SEQNUM = 1;
	final static int MAX_SEQNUM = 65535;
	
	//rtsp states
	final static int INIT = 0;
	final static int READY = 1;
	final static int PLAYING = 2;
	static int state; //RTSP state == INIT or READY or PLAYING

	//input and output stream filters
	static BufferedReader RTSPBufferedReader;
	static BufferedWriter RTSPBufferedWriter;

	static String VideoFileName; //video file to request to the server
	int RTSPSeqNb = 0; //Sequence number of RTSP messages within the session
	int RTSPid = 0; //ID of the RTSP session (given by the RTSP Server)

	final static String CRLF = "\r\n";

	//--------------------------
	//Constructor
	//--------------------------
	public RTSP(String ServerHost, int RTSP_PORT, int RTP_PORT, String VideoFileName) {
    this.ServerHost = ServerHost;
    this.RTSP_PORT = RTSP_PORT;
    this.RTP_PORT = RTP_PORT;
    this.VideoFileName = VideoFileName;

    //first state
    state = INIT;

    try {
      //Establish a TCP connection with the server to exchange RTSP messages
      InetAddress ServerIPAddr = InetAddress.getByName(ServerHost);
      RTSPsocket = new Socket(ServerIPAddr, RTSP_PORT);

      //Set input and output stream filters:
      RTSPBufferedReader = new BufferedReader(new InputStreamReader(RTSPsocket.getInputStream()) );
      RTSPBufferedWriter = new BufferedWriter(new OutputStreamWriter(RTSPsocket.getOutputStream()) );

    } catch (Exception e) {
      System.out.println(e + " " + ServerHost + RTSP_PORT);
    }
  }

	//------------------------------------
	//Parse Server Response
	//------------------------------------
	private int parse_response() {
		int reply_code = 0;

		try {
			//parse status line and extract the reply_code:
			String StatusLine = RTSPBufferedReader.readLine();
			System.out.println("RTSP Client - Received from Server:");
			System.out.println(StatusLine);

			StringTokenizer tokens = new StringTokenizer(StatusLine);
			tokens.nextToken(); //skip over the RTSP version
			reply_code = Integer.parseInt(tokens.nextToken());

			//if reply code is OK get and print the 2 other lines
			if (reply_code == 200) {
				String SeqNumLine = RTSPBufferedReader.readLine();
				System.out.println(SeqNumLine);

				String SessionLine = RTSPBufferedReader.readLine();
				System.out.println(SessionLine);

				//if state == INIT gets the Session Id from the SessionLine
				tokens = new StringTokenizer(SessionLine);
				tokens.nextToken(); //skip over the Session:
				RTSPid = Integer.parseInt(tokens.nextToken());
			}

		} catch (Exception ex) {
			System.out.println("Exception caught RTSP: " + ex);
			System.exit(0);
		}

		return(reply_code);
	}

	//------------------------------------
	//Send RTSP Request
	//------------------------------------
	//TO COMPLETE
	//.............
	public void send_request(String request_type) {
		try {
			if((request_type.compareTo("SETUP")==0)&& state==INIT) { //se comprueba para cada caso que la petición y el estado son correspondientes
				//se envía la petición
				RTSPBufferedWriter.write("SETUP rtsp://"+this.ServerHost+"/"+this.VideoFileName+" RTSP/1.0"+CRLF);
				RTSPSeqNb=(int) (Math.random()*(MAX_SEQNUM-MIN_SEQNUM)+MIN_SEQNUM);
				RTSPBufferedWriter.write("CSeq: " + Integer.toString(RTSPSeqNb)+CRLF);
				RTSPBufferedWriter.write("Transport: RTP/UDP; client_port= "+Integer.toString(RTP_PORT)+CRLF);
				RTSPBufferedWriter.flush();
				//se espera una respuesta y se comprueba que es correcta. Si es así, se actualeza CSeq y se cambia de estado
				int parsed_response=parse_response();
				if (parsed_response==200) {
					RTSPSeqNb++;
				    state=READY;
				    System.out.println("New RTSP state: READY");
				} else {
					System.out.println("Error: "+ parsed_response);
					System.exit(0);
				}
				
			} else if (request_type.compareTo("PLAY")==0 && state==READY){
				RTSPBufferedWriter.write("PLAY rtsp://"+this.ServerHost+"/"+this.VideoFileName+" RTSP/1.0"+CRLF);
				RTSPBufferedWriter.write("CSeq: " + Integer.toString(RTSPSeqNb)+CRLF);
				RTSPBufferedWriter.write("Session: "+RTSPid+CRLF);
				RTSPBufferedWriter.flush();
				
				int parsed_response=parse_response();
				if (parsed_response==200) {
					RTSPSeqNb++;
				    state=PLAYING;
				    System.out.println("New RTSP state: PLAYING");

				} else {
					System.out.println("Error: "+ parsed_response);
					System.exit(0);
				}
			} else if(request_type.compareTo("PAUSE")==0 && state==PLAYING){
				RTSPBufferedWriter.write("PAUSE rtsp://"+this.ServerHost+"/"+this.VideoFileName+" RTSP/1.0"+CRLF);
				RTSPBufferedWriter.write("CSeq: " + Integer.toString(RTSPSeqNb)+CRLF);
				RTSPBufferedWriter.write("Session: "+RTSPid+CRLF);
				RTSPBufferedWriter.flush();
				
				int parsed_response=parse_response();
				if (parsed_response==200) {
					RTSPSeqNb++;
				    state=READY;
				    System.out.println("New RTSP state: READY");
				} else {
					System.out.println("Error: "+ parsed_response);
					System.exit(0);
				}
			}else if(request_type.compareTo("TEARDOWN")==0 && (state==PLAYING || state==READY)){
				RTSPBufferedWriter.write("TEARDOWN rtsp://"+this.ServerHost+"/"+this.VideoFileName+" RTSP/1.0"+CRLF);
				RTSPBufferedWriter.write("CSeq: " + Integer.toString(RTSPSeqNb)+CRLF);
				RTSPBufferedWriter.write("Session: "+RTSPid+CRLF);
				RTSPBufferedWriter.flush();
				
				System.out.println("antes del parse");
				int parsed_response=parse_response();
				if (parsed_response==200) {
					RTSPSeqNb++;
				    state=INIT;
				    System.out.println("New RTSP state: INIT");
				} else {
					System.out.println("Error: "+ parsed_response);
					System.exit(0);
				}
				System.out.println("despues del parse");
			} else {
				System.out.println("Please, select a valid a option.");
			}

		} catch (Exception ex) {
			System.out.println("Exception caught: " + ex);
			System.exit(0);
		}
	}
	

}//end of Class RTSP
