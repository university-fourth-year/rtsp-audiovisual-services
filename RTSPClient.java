import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.SwingUtilities;

import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;

public class RTSPClient{
	
    private final JFrame frame;
    
    private final EmbeddedMediaPlayerComponent mediaPlayerComponent;
        
    private RTSP rtsp_sess;
    
    private boolean notSetUp; //booleana que se usará para que, una vez se ha hecho set-up, no se pueda volver a hacer hasta que no se haga stop antes
    
    private String IP;		//IP o nombre de destino
    private String IPLocal; //IP local que se utilizará para poder reproducir el contenido que llega al cliente
    
    private InetAddress destAddress; //Se usará para sacar la IP de destino a partir de la variable IP
    
    public static void main(final String[] args) {
        new NativeDiscovery().discover();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new RTSPClient(args);
            }
        });
    }
    
    public RTSPClient(String[] args) {
        
        
        frame = new JFrame("Media Player");
        notSetUp=true;
        rtsp_sess=null;
        
        //TO DO! choose the correct arguments for the methods below. Add more method calls as necessary
        frame.setBounds(100, 100, 600, 400);
        
        
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                mediaPlayerComponent.release();
                System.exit(0);
            }
        });
        
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        
        mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
        contentPane.add(mediaPlayerComponent, BorderLayout.CENTER);
        
        JPanel controlsPane = new JPanel();
        
        //Creación de botones y elementos de la IU
        
        //----------------------
        JButton playButton = new JButton("Play");
        controlsPane.add(playButton);
        JButton pauseButton = new JButton("Pause");
        controlsPane.add(pauseButton);
        JButton stopButton = new JButton("Stop");
        controlsPane.add(stopButton);
        JButton setUpButton= new JButton("Set Up");
        controlsPane.add(setUpButton);
        contentPane.add(controlsPane, BorderLayout.SOUTH);
        
        JPanel textFieldPane= new JPanel();
        JTextField newURL = new JTextField("Introduce URL:",50);
        textFieldPane.add(newURL);
        contentPane.add(textFieldPane, BorderLayout.PAGE_START);
                
        //Procedemos a crea todos los handlers de los botones, en los cuales se realizará el "request" correspondiente para cada botón
        //-----------------------
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TO DO!! configure the playback of the video received via RTP, or resume a paused playback.
            	if(rtsp_sess!=null) {
        			rtsp_sess.send_request("PLAY");
        		}
            	try {
            		//sacamos la IP destino
                    destAddress=InetAddress.getByName(IP);
                    IPLocal=Inet4Address.getLocalHost().getHostAddress();
            	} catch (UnknownHostException ee) {
                	System.out.println("Couldn't find IP address for: "+IPLocal);
                }
            	//comenzamos a reproducir lo que nos llega por el puerto 5004 (RTP)
            	if (!notSetUp) mediaPlayerComponent.getMediaPlayer().playMedia("rtp://@"+IPLocal+":5004");
            }
        });
        
        //TO DO! implement a PAUSE button to pause video playback.
            pauseButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		if(rtsp_sess!=null) {
        			rtsp_sess.send_request("PAUSE");
        		}

        	}
        });
        
        //TO DO! implement a STOP button to stop video playback and exit the application.
        stopButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		if(rtsp_sess!=null) {
        			rtsp_sess.send_request("TEARDOWN");
        		}
        		//Se cambia el valor de notSetUp para que se pueda volver a crear nuevas sesiónes de RTP y RTSP
        		notSetUp=true;
        	}
        });
        setUpButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String url = newURL.getText();
        		String[] split=url.split("/");
        		String[] split2=split[2].split(":");
        		IP=split2[0];
        		int RTSP_PORT=Integer.parseInt(split2[1]);
        		String file=split[3];
        		if (notSetUp) {
        			//Se crea una nueva sesión RTSP
            		rtsp_sess= new RTSP (IP, RTSP_PORT, 5004, file);
            		notSetUp=false;
        		}
        		
        		rtsp_sess.send_request("SETUP");
        	}
        });
        //Makes visible the window
        frame.setContentPane(contentPane);
        frame.setVisible(true);
    }
    
    
}

